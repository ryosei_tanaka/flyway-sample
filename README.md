# README #

Flyway sample

#### Install
* Flyway 4.2.0
* H2 Database 1.4.196

#### Setup

run h2 server

```
#!Java

java -cp h2*.jar org.h2.tools.server
```